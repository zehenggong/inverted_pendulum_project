import os
import sys

import gymnasium as gym
import matplotlib.pyplot as plt
import numpy as np
import torch
from gymnasium.envs.classic_control.resettable_pendulum import initialize_pendulum


sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from algorithms.customized_sac import Actor, ModelCheckpoint, make_env
from gymnasium.envs import classic_control

from torch.utils.tensorboard import SummaryWriter

# Load the environment and trained model
target_angle = 0
penalty = 0.0
seed = 1
# run_name="RRPendulum-v0__penaltyExperiments/Angle45/Penalty0.__1__1701710310"
# run_name = "RRPendulum-v0__penaltyExperiments/Angle35/Penalty0.__1__1701444768"
# run_name = "RRPendulum-v0__penaltyExperiments/Angle35/Penalty0.5__1__1701708771"
# run_name = "RRPendulum-v0__penaltyExperiments/Angle35/Penalty0.__1__1701874741"
run_name = "RRPendulum-v0__penaltyExperiments/Angle0/Penalty0.__1__1701712224"

envs = gym.vector.SyncVectorEnv(
    [
        make_env(
            "RRPendulum-v0",
            seed=seed,
            idx=0,
            capture_video=True,
            run_name=run_name,
            penalty=penalty,
            angle=target_angle,
            noise_level=0,
            eval=True,
        )
    ]
)
# Initialize the environment
# env = gym.make(
#     "RRPendulum-v0",
#     penalty=penalty,
#     angle=target_angle,
#     noise_level=0,
#     seed=seed,
#     render_mode="rgb_array",
# )

# env = gym.wrappers.RecordVideo(env, f"eval_videos/{run_name}")

# env = initialize_pendulum(angle=target_angle)
device = torch.device("cpu")
actor = Actor(envs).to(device)
# actor = Actor(env).to(device)

checkpoint = ModelCheckpoint(checkpoint_dir="model_checkpoints")
writer = SummaryWriter(log_dir=f"eval_logs/{run_name}")
for step in range(49900, 50000, 10000):
    print("Step:", step)

    actor = checkpoint.load(
        actor,
        "{frun_name}/actor_model_step_{fstep}.pt".format(
            frun_name=run_name, fstep=step
        ),
    )
    # Set the model to evaluation mode
    actor.eval()
    # Define the directory to save the plot
    save_dir = "eval_figures/Angle{ftarget_angle}/{frun_name}/".format(
        ftarget_angle=target_angle, frun_name=run_name
    )

    # Create the directory if it does not exist
    os.makedirs(save_dir, exist_ok=True)

    # Print shapes after loading
    print("Shape of action_scale after loading:", actor.action_scale.shape)
    print("Shape of action_bias after loading:", actor.action_bias.shape)

    # obss, _ = envs.reset(seed=seed)
    obs, _ = envs.reset(seed=seed)


    thetas = [np.arctan2(obs[0][1], obs[0][0])]
    angular_velocities = [obs[0][2]]

    def angle_normalize(x):
        return ((x + np.pi) % (2 * np.pi)) - np.pi

    rewards = []
    episodic_return = 0 
    episode_num = 0

    i = 0
    time_steps = [i]
    terminated = False
    # while not terminated:
    while i < 299:
        i += 1
        if i % 100 == 0:
            print(f"Loop iteration: {i}")
        actions, _, _ = actor.get_action(torch.Tensor(obs).to(device))
        actions = actions.detach().cpu().numpy()

        print("Action:", actions)
        obs, reward, terminated, _, _ = envs.step(np.array(actions))

        # print("obs after step:", obss[0])
        theta = np.arctan2(obs[0][1], obs[0][0])
        theta = theta * 180 / np.pi
        angular_velocity = obs[0][2] * 180 / np.pi

        time_steps.append(i)
        thetas.append(theta)

        angular_velocities.append(angular_velocity)
        episodic_return += reward
        if terminated:
            episode_num += 1
            print(f"Episodic Return: {episodic_return}") 
            writer.add_scalar("Episodic Return", episodic_return, episode_num)
            # obss, _ = envs.reset(seed=seed)  
            episodic_return = 0
        rewards.append(reward)

    envs.close()
    writer.close()

    print("Angular velocities:", angular_velocities)
    print("Thetas:", thetas)
    print("Rewards:", rewards)

    plt.figure(figsize=(8, 8))
    plt.scatter(thetas, angular_velocities, s=9)
    plt.title("Phase Diagram for Pendulum")
    plt.xlabel("Angular Position (Degrees)")
    plt.ylabel("Angular Velocity (Degrees/s)")
    plt.grid(True)
    plt.savefig(
        os.path.join(
            save_dir, "phase_diagram_actor_model_step_{fstep}.png".format(fstep=step)
        ),
        dpi=300,
    )
    plt.show()

    # Plot the Pendulum Position
    plt.figure(figsize=(8, 8))
    plt.scatter(time_steps, thetas, s=9)
    # plt.plot(timesteps, thetas)
    plt.title("Pendulum Position")
    plt.xlabel("Timestep")
    plt.ylabel("Angular Position (Degrees)")
    plt.grid(True)
    plt.savefig(
        os.path.join(
            save_dir, "trajectories_actor_model_step_{fstep}.png".format(fstep=step)
        ),
        dpi=300,
    )
    plt.show()
