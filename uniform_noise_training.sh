penalty=10.0
noise_level=0.1
angle=40
seed=1
exp_name="Noise${noise_level}_angle${angle}_penalty${penalty}"

echo "Running experiment with penalty: $penalty, noise: $noise_level"
poetry run python inverted_pendulum/algorithms/customized_sac.py \
    --exp-name $exp_name\
    --seed $seed \
    --env-id RRUniformNoisePendulum-v0 \
    --total-timesteps 80001 \
    --alpha 0 \
    --autotune False \
    --penalty $penalty \
    --angle $angle \
    --noise-level $noise_level
