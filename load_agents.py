from inverted_pendulum.algorithms.customized_sac import ModelCheckpoint, Actor, make_env
from gymnasium.envs.classic_control.resettable_pendulum import initialize_pendulum, resettable_pendulum_with_noise, resettable_pendulum_with_uniform_noise
import torch
import os
from torch.utils.tensorboard import SummaryWriter
import gymnasium as gym
import argparse
from distutils.util import strtobool

def parse_args():
    # fmt: off
    parser = argparse.ArgumentParser()
    parser.add_argument("--exp-name", type=str, default=os.path.basename(__file__).rstrip(".py"),
        help="the name of this experiment")
    parser.add_argument("--seed", type=int, default=1,
        help="seed of the experiment")
    parser.add_argument("--torch-deterministic", type=lambda x: bool(strtobool(x)), default=True, nargs="?", const=True,
        help="if toggled, `torch.backends.cudnn.deterministic=False`")
    parser.add_argument("--cuda", type=lambda x: bool(strtobool(x)), default=True, nargs="?", const=True,
        help="if toggled, cuda will be enabled by default")
    parser.add_argument("--track", type=lambda x: bool(strtobool(x)), default=False, nargs="?", const=True,
        help="if toggled, this experiment will be tracked with Weights and Biases")
    parser.add_argument("--wandb-project-name", type=str, default="cleanRL",
        help="the wandb's project name")
    parser.add_argument("--wandb-entity", type=str, default=None,
        help="the entity (team) of wandb's project")
    parser.add_argument("--capture-video", type=lambda x: bool(strtobool(x)), default=False, nargs="?", const=True,
        help="whether to capture videos of the agent performances (check out `videos` folder)")

    # Algorithm specific arguments
    parser.add_argument("--env-id", type=str, default="Hopper-v4",
        help="the id of the environment")
    parser.add_argument("--total-timesteps", type=int, default=1000000,
        help="total timesteps of the experiments")
    parser.add_argument("--buffer-size", type=int, default=int(1e6),
        help="the replay memory buffer size")
    parser.add_argument("--gamma", type=float, default=0.99,
        help="the discount factor gamma")
    parser.add_argument("--tau", type=float, default=0.005,
        help="target smoothing coefficient (default: 0.005)")
    parser.add_argument("--batch-size", type=int, default=256,
        help="the batch size of sample from the reply memory")
    parser.add_argument("--learning-starts", type=int, default=5e3,
        help="timestep to start learning")
    parser.add_argument("--policy-lr", type=float, default=3e-4,
        help="the learning rate of the policy network optimizer")
    parser.add_argument("--q-lr", type=float, default=1e-3,
        help="the learning rate of the Q network network optimizer")
    parser.add_argument("--policy-frequency", type=int, default=2,
        help="the frequency of training policy (delayed)")
    parser.add_argument("--target-network-frequency", type=int, default=1, # Denis Yarats' implementation delays this by 2.
        help="the frequency of updates for the target nerworks")
    parser.add_argument("--noise-clip", type=float, default=0.5,
        help="noise clip parameter of the Target Policy Smoothing Regularization")
    parser.add_argument("--alpha", type=float, default=0.2,
            help="Entropy regularization coefficient.")
    parser.add_argument("--autotune", type=lambda x:bool(strtobool(x)), default=True, nargs="?", const=True,
        help="automatic tuning of the entropy coefficient")
    
    # RRPendulum-v0 specific arguments
    parser.add_argument("--penalty", type=float, default=0,
        help="Penalty in the reward function.")
    parser.add_argument("--angle", type=float, default=0,
        help="Target angle for the pendulum in degrees.")
    
    # RRNoisePendulum-v0 specific arguments
    parser.add_argument("--noise-level", type=float, default=0,
    help="Noise level of Gaussian noise added to action.")

    parser.add_argument("--exploration-level", type=float, default=None,
    help="Noise level of Gaussian noise added to action during exploration.")

    # Evaluation specific arguments
    parser.add_argument("--actor-path", type=str, default=None,
    help="Model Checkpoint path.")

    args = parser.parse_args()
    # fmt: on
    return args

args = parse_args()
print(args.actor_path)
# env setup
envs = gym.vector.SyncVectorEnv(
    [
        make_env(
            args.env_id,
            args.seed,
            0,
            args.capture_video,
            args.exp_name,
            args.penalty,
            args.angle,
            args.noise_level,
            eval=True,
        )
    ]
)
assert isinstance(
    envs.single_action_space, gym.spaces.Box
), "only continuous action space is supported"

# Load the Trained Actor Model
checkpoint = ModelCheckpoint(checkpoint_dir='model_checkpoints')
device = torch.device("cpu")

actor = Actor(envs).to(device)
actor = checkpoint.load(actor, args.actor_path)

# device = torch.device("cuda" if torch.cuda.is_available() and args.cuda else "cpu")
actor.to(device)

# Initialize SummaryWriter for logging
eval_run_name = f"{args.env_id}__{args.exp_name}__{args.seed}"
writer = SummaryWriter(f"runs/evaluation_run/{eval_run_name}")

# Evaluate the Actor in the New Environment
obs, _ = envs.reset()
terminated = False
episode_rewards = 0
episode_length = 0

while not terminated:
    obs_tensor = torch.tensor(obs, dtype=torch.float32).to(device)
    with torch.no_grad():
        action, _, _= actor.get_action(obs_tensor)
    next_obs, reward, terminated, _, _ = envs.step(action.cpu().numpy()[0])
    episode_rewards += reward
    episode_length += 1
    obs = next_obs
    if episode_length >=500:
        terminated = True
    if terminated:
        writer.add_scalar("evaluation/episodic_return", episode_rewards, episode_length)
        writer.add_scalar("evaluation/episodic_length", episode_length, episode_length)
        print(f"Episode finished with reward: {episode_rewards}, length: {episode_length}")
        # Reset for next episode
        obs, _ = envs.reset()
        episode_rewards = 0
        episode_length = 0

writer.close()
