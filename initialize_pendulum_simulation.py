import logging

import gymnasium as gym
import numpy as np
from generalized_wrappers import NoiseWrapper, TabularPendulumWrapper
from resettable_pendulum import initialize_pendulum  # set the angle to 0!


logging.basicConfig(level=logging.INFO, format="%(message)s")


def create_custom_env():
    env = initialize_pendulum()
    env = NoiseWrapper(
        env, noise_level=0.04
    )  # small_noise=0.04, moderate_noise=0.20, high_noise=0.40

    # Apply the TabularPendulumWrapper (specific to pendulum environment)
    theta_bins = np.linspace(-np.pi, np.pi, num=180)
    theta_dot_bins = np.linspace(-8, 8, num=180)
    action_bins = np.linspace(-2, 2, num=20)

    env = TabularPendulumWrapper(env, theta_bins, theta_dot_bins, action_bins)
    return env


env = create_custom_env()


def p_map(sa, p):
    env.reset()
    env.state = sa[:2]
    action = sa[2]
    # logging.info(f"Action before step: {action}, type: {type(action)}, shape: {np.shape(action)}")
    if not isinstance(action, np.ndarray):
        action = np.array([action])
    next_state, _, terminated, _, _ = env.step(action)
    next_state = np.arccos(next_state[0])

    return (next_state, terminated)


def sa2xp(sa, p):
    return sa, p


def xp2s(state, p):
    return state, p
