import logging
import unittest

import gymnasium as gym
import numpy as np
from gymnasium.wrappers.generalized_wrappers import (
    NoiseWrapper,
    RewardAdjustmentWrapper,
    TabularPendulumWrapper,
)


logging.disable(logging.CRITICAL)


class TestRewardAdjustmentWrapper(unittest.TestCase):
    def setUp(self):
        self.original_env = gym.make("Pendulum-v1")

        def termination_condition(observation):
            # Terminate if the pendulum is swinging
            return observation[2] != 0

        def angle_normalize(x):
            return ((x + np.pi) % (2 * np.pi)) - np.pi

        def pendulum_reward_function(th, thdot, action, angle):
            print("info:", angle_normalize(th), angle_normalize(angle))
            reward = -(
                (angle_normalize(th) - angle_normalize(angle)) ** 2
                + 0.1 * thdot**2
                + 0.001 * (action**2)
            )
            return reward

        self.env = RewardAdjustmentWrapper(
            self.original_env,
            penalty=10,
            terminatiton_condition=termination_condition,
            reward_function=pendulum_reward_function,
            angle=np.pi / 2,
        )

    def test_penalty_application(self):
        self.original_env.reset()
        self.env.reset()
        action = [0.0]
        _, reward, terminated, _, _ = self.env.step(action)
        self.assertTrue(terminated, "The environment should not have terminated")
        action = [0.1]
        _, reward, terminated, _, _ = self.env.step(action)
        self.assertTrue(terminated, "The environment should have terminated")
        self.assertEqual(
            reward,
            self.env.original_reward - 10,
            "The penalty should be subtracted from the original reward",
        )

    def test_angle_application(self):
        self.env.reset()
        action = [0.1]
        th, thdot = self.env.state
        _, reward, terminated, _, _ = self.env.step(action)
        if not terminated:
            self.assertEqual(
                reward,
                -(
                    (angle_normalize(th) - angle_normalize(np.pi / 2)) ** 2
                    + 0.1 * thdot**2
                    + 0.001 * (action[0] ** 2)
                ),
            )
        else:
            self.assertEqual(
                reward,
                -(
                    (angle_normalize(th) - angle_normalize(np.pi / 2)) ** 2
                    + 0.1 * thdot**2
                    + 0.001 * (action[0] ** 2)
                )
                - 10,
            )


class TestNoiseWrapper(unittest.TestCase):
    def setUp(self):
        env = gym.make("Pendulum-v1")
        self.noise_level = 0.1
        self.env = NoiseWrapper(env, noise_level=self.noise_level)

    def test_noise_addition(self):
        np.random.seed(0)
        self.env.reset()
        original_action = [0.0]
        _, _, _, _, _ = self.env.step(original_action)
        noisy_action_first_run = self.env.last_action

        # Reset and repeat with the same seed
        np.random.seed(0)
        self.env.reset()
        _, _, _, _, _ = self.env.step(original_action)
        noisy_action_second_run = self.env.last_action

        # Check if the noisy actions are identical
        self.assertNotEqual(
            noisy_action_first_run,
            noisy_action_second_run,
            "Noisy actions should not be identical with a fixed seed",
        )
        self.env.reset()
        original_action = [0.0]
        _, _, _, _, _ = self.env.step(original_action)
        noisy_action = self.env.last_action

        # Check if the noisy action is within expected bounds
        noise_lower_bound = (
            original_action[0] - 3 * self.noise_level
        )  # 3-sigma rule for Gaussian
        noise_upper_bound = original_action[0] + 3 * self.noise_level

        self.assertTrue(
            noise_lower_bound <= noisy_action <= noise_upper_bound,
            "Noisy action should be within 3 standard deviations of the original action",
        )


class TestTabularPendulumWrapper(unittest.TestCase):
    def setUp(self):
        env = gym.make("Pendulum-v1")
        theta_bins = np.linspace(-np.pi, np.pi, num=180)
        theta_dot_bins = np.linspace(-8, 8, num=180)
        action_bins = np.linspace(-2, 2, num=20)
        self.env = TabularPendulumWrapper(env, theta_bins, theta_dot_bins, action_bins)

    def test_discretization(self):
        self.env.reset()
        action = [0.0]
        discrete_observation, _, _, _, _ = self.env.step(action)
        theta = np.arccos(
            self.env.continuous_observation[0]
        )  # Convert from Cartesian to polar coordinates
        theta_dot = self.env.continuous_observation[2]  # Angular velocity

        # Expected discretized values
        expected_discrete_theta = self.env.theta_bins[
            self.env.discretize(theta, self.env.theta_bins)
        ]
        expected_discrete_theta_dot = self.env.theta_dot_bins[
            self.env.discretize(theta_dot, self.env.theta_dot_bins)
        ]
        expected_discrete_observation = np.array(
            [
                np.cos(expected_discrete_theta),
                np.sin(expected_discrete_theta),
                expected_discrete_theta_dot,
            ],
            dtype=np.float32,
        )

        # Check if the discretization matches the expected values
        np.testing.assert_array_almost_equal(
            discrete_observation,
            expected_discrete_observation,
            decimal=5,
            err_msg="Discretization did not match the expected values",
        )


def angle_normalize(x):
    return ((x + np.pi) % (2 * np.pi)) - np.pi


if __name__ == "__main__":
    unittest.main()
