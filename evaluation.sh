angle=40
penalty=10.0
# Define a list of noise levels for evaluation
noise_levels=(0 0.1 0.2 0.3 0.4)
seed=5
training_noises=(0 0.1 0.2 0.3 0.4)

for training_noise in "${training_noises[@]}"
do
    for noise_level in "${noise_levels[@]}"
    do
        actor_path="RRUniformNoisePendulum-v0__Exploration0.1TrainingNoise${training_noise}_angle40_penalty10.0__${seed}__1705667783/actor_model_step_80000.pt"
        exp_name="TrainingNoise${training_noise}_EvalNoise${noise_level}_angle${angle}_penalty${penalty}_seed${seed}"

        echo "Running Evaluation with angle: $angle noise_level: $noise_level and penalty: $penalty"
        poetry run python load_agents.py \
            --exp-name $exp_name\
            --seed $seed \
            --env-id RRUniformNoisePendulum-v0 \
            --total-timesteps 80001 \
            --alpha 0 \
            --autotune False \
            --capture-video \
            --penalty $penalty \
            --angle $angle \
            --noise-level $noise_level \
            --actor-path $actor_path
    done
done
