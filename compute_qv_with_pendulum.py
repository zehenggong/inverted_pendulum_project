# compute_qv_with_pendulum.py
import initialize_pendulum_simulation as pendulum_tabular
import numpy as np
import vibly.viability as vibly


if __name__ == "__main__":
    # Create the p_map parameter object
    p = {
        "max_speed": 8,
        "max_torque": 2.0,
        "dt": 0.05,
        "g": 9.81,
        "m": 1.0,
        "m": 1.0,
    }

    p_map = pendulum_tabular.p_map
    p_map.p = p
    p_map.sa2xp = pendulum_tabular.sa2xp
    p_map.xp2s = pendulum_tabular.xp2s

    # Define the discretization for the state space
    theta_bins = np.linspace(-np.pi, np.pi, num=180)  # Discretize angle
    theta_dot_bins = np.linspace(-8.0, 8.0, num=180)  # Discretize angular velocity
    # Define the discretization for the action space
    action_bins = np.linspace(-2.0, 2.0, num=20)  # Discretize torque
    # Define the grids for the state and action spaces
    s_grid = (theta_bins, theta_dot_bins)
    a_grid = (action_bins,)

    grids = {"states": s_grid, "actions": a_grid}

    Q_map, Q_F = vibly.parcompute_Q_map(grids, p_map)
    Q_V, S_V = vibly.compute_QV(Q_map, grids)
